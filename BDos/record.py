from multiprocessing import Process, Manager
from multiprocessing.managers import BaseManager
from ip import ip_address
import random

node_count = 4

def recordData(number):
    with open("./data.csv", "a+") as f:
        f.write(number + "\n")

def clear():
    with open('./data.csv', 'w') as f:
        f.truncate()

if __name__ == '__main__':
    mgr = BaseManager(address=(ip_address,50000), authkey=b'pwd')
    mgr.register("BlockBegin")
    mgr.register("BlockMiner")
    mgr.register("ev")
    mgr.register("BDos")
    mgr.connect()

    blockbegin = mgr.BlockBegin()
    blockminer = mgr.BlockMiner()
    ev = mgr.ev()
    conflict = mgr.conflict()

    clear()
    for i in range(10000):
        print("this is number: ",i)
        conflict.append(random.random())
        if eval(str(conflict))[-1] < 0.15:
            print("BDos")
        while str(ev) != '[]':            #清空消息
            ev.pop()
        blockbegin.pop()
        blockbegin.append('begin')   #重置开始标志
        while str(blockminer) == '[]':  #等待有人挖到
            print(str(blockbegin), str(blockbegin) == '[\'wait\']')
        miner_list = str(blockminer).replace(' ', '').replace("'", '')
        print(miner_list)
        recordData(miner_list[1:-1].split(",")[-1])

        while len(list(str(ev)[1:-1].split(','))) != node_count:      # 等待所有人都接收到挖矿成功的消息
            if str(list(str(ev)[1:-1].split(',')))== "['']":
                continue
            print(list(str(ev)[1:-1].split(',')))
        conflict.pop()
        # blockdetail.pop()