## DISTRIBUTE BWH ATTACK MODEL
```c
用一个标志位表示本轮是否收到BDos攻击，按攻击者算力比例的概率发生；
如果发生BDos攻击，小矿工停止挖矿，大矿工继续在主链上挖，并在挖到后产生分叉竞争
```
### File discription

- Blockchain.py  账本，每个进程都各自维护
- main.py  暂时没用到
- manager.py 三层架构最顶层，中心服务器，控制共享变量及挖矿开始
- Master.py 中间层，负责挖矿管理底层Miner
- Miner.py 最底层挖矿进程，相当于单个矿工
- record.py 管理开始结束及中间挖到矿的等待

- ./distribute_demo 看起来没用

### Usage

1. 启动manager 初始化服务器及数据
2. 启动Master... 开启挖矿进程

3. 启动record  开始挖矿
