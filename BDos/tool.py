
with open("BWH/times.csv", "r") as p1 , open("./Pi2/times.csv") as p2, open("./Pi3/times.csv") as p3:
    sum_p1 = 0
    count_p1 = 0
    for line in p1.readlines():
        count_p1 += 1
        sum_p1 += int(line)
    sum_p2 = 0
    count_p2 = 0
    for line in p2.readlines():
        count_p2 += 1
        sum_p2 += int(line)
    sum_p3 = 0
    count_p3 = 0
    for line in p3.readlines():
        count_p3 += 1
        sum_p3 += int(line)
    print("p1:")
    print(count_p1, sum_p1, round(sum_p1 / count_p1, 2))
    print("p2:")
    print(count_p2, sum_p2, round(sum_p2 / count_p2, 2))
    print("p3:")
    print(count_p3, sum_p3, round(sum_p3 / count_p3, 2))
    print("all:")
    print(count_p1 + count_p2 + count_p3, sum_p1 + sum_p2 + sum_p3, round((sum_p1 + sum_p2 + sum_p3) / (count_p1 + count_p2 + count_p3), 2))