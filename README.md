## DISTRIBUTE BWH ATTACK MODEL simuBits

### Note
每个节点的核心代码基本一致，所以阅读Pi1、Pi2、Pi3、Pi4任意一个节点即可。





### File discription

- Blockchain.py  账本，每个进程都各自维护
- manager.py 三层架构最顶层，中心服务器，控制共享变量及挖矿开始
- Master.py 中间层，负责挖矿管理底层Miner
- Miner.py 最底层挖矿进程，相当于单个矿工
- record.py 管理开始结束及中间挖到矿的等待


### Usage

1. 启动manager 初始化服务器及数据
   1. python manager.py
2. 启动各个节点的Master... 开启挖矿进程
   - cd Pi1;python Master.py
   - cd Pi2;python Master.py
   - cd Pi3;python Master.py

3. 启动record  开始挖矿


 