'''
考虑三层架构，当前文件为顶层，是作为分布式管理的manager；
Master是单个机器运行的
Miner是Master下开出来的进程

网络传输也需要成本，很难考量速度的得失
'''

from multiprocessing.managers import BaseManager as bm
from Blockchain import Blockchain
from ip import ip_address


# 注册的共享变量
BlockBegin = ['wait']
def return_begin():
    return BlockBegin

BlockMiner = []

def return_miner():
    return BlockMiner

ev_miner = []
def return_ev():
    return ev_miner

BlockDetail = []
def return_detail():
    return BlockDetail


if __name__ == '__main__':
    # 因为分布式共享变量的特殊性，他没法直接修改或者直接读取，因为其地址是固定的，所以每次都需要pop/push来读取修改
    bm.register("BlockBegin", callable=return_begin)    #用来控制当前系统的状态是等待wait；还是begin开始挖矿
    bm.register("BlockMiner", callable=return_miner)  #记录挖矿信息，现在作用相当于链
    bm.register("ev",callable=return_ev)             #记录所有矿工都接收到消息，因为如果一个矿工挖完了直接开始挖下一个，则因为网络速度对其他矿工不公平，并且真实情况也是要大家验证并协商一致
    bm.register("BlockDetail",callable=return_detail) # 保存区块信息
    mgr = bm(address=(ip_address,50000), authkey=b'pwd')
    server = mgr.get_server()
    server.serve_forever()

    # server = mgr.get_server()
    # server.serve_forever()
