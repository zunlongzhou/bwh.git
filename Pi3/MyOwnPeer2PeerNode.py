#######################################################################################################################
# Author: Maurice Snoeren                                                                                             #
# Version: 0.2 beta (use at your own risk)                                                                            #
#                                                                                                                     #
# MyOwnPeer2PeerNode is an example how to use the p2pnet.Node to implement your own peer-to-peer network node.        #
# 28/06/2021: Added the new developments on id and max_connections
#######################################################################################################################
from p2pnetwork.node import Node
from utils import read_config
config_file = "config.ini"

def modifyConfig(section , type, data="127.0.0.1"):
    config = read_config(config_file)
    config.set("info", type, ip)
    with open(config_file, "w+") as f:
        config.write(f)



class MyOwnPeer2PeerNode (Node):

    # Python class constructor
    def __init__(self, host, port, id=None, callback=None, max_connections=0):
        super(MyOwnPeer2PeerNode, self).__init__(host, port, id, callback, max_connections)
        print("MyPeer2PeerNode: Started")

    # all the methods below are called when things happen in the network.
    # implement your network node behavior to create the required functionality.

    def outbound_node_connected(self, node):
        print("outbound_node_connected (" + self.id + "): " + node.id)
        
    def inbound_node_connected(self, node):
        print("inbound_node_connected: (" + self.id + "): " + node.id)

    def inbound_node_disconnected(self, node):
        print("inbound_node_disconnected: (" + self.id + "): " + node.id)

    def outbound_node_disconnected(self, node):
        print("outbound_node_disconnected: (" + self.id + "): " + node.id)

    def node_message(self, node, data):
        print("node_message (" + self.id + ") from " + node.id + ": " + str(data))
        data = data.split()
        if data[0] == "changeIP":
            modifyConfig("info", "ip", data[1])
        elif data[0] == "bwh":
            modifyConfig("status", "bwh", "1")

        
    def node_disconnect_with_outbound_node(self, node):
        print("node wants to disconnect with oher outbound node: (" + self.id + "): " + node.id)
        
    def node_request_to_stop(self):
        print("node is requested to stop (" + self.id + "): ")

        
