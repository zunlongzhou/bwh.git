'''
保存数据的脚本
'''
import os
import shutil

def mycopyfile(fileUrl, srcfile, dstpath): # 复制函数
    if not os.path.exists(dstpath):
        os.makedirs(dstpath)                       # 创建路径
    shutil.copy(fileUrl + srcfile, dstpath + srcfile)          # 复制文件

baseUrl = ".."
saveUrl = "/saveData"
# 目标文件夹名，如 500-BWH
newFolder = "/test"

searchFolder = ["/Pi1", "/Pi2", "/Pi3", "/Pi4"]
searchFile = ["/blockchain.txt", "/income.txt", "/times.csv", "/ledger.txt"]

for Folder in searchFolder:
    url = baseUrl + Folder
    resUrl = baseUrl + saveUrl + newFolder + Folder
    for filename in searchFile:
        mycopyfile(url, filename, resUrl)

mycopyfile("..", "/data.csv", baseUrl + saveUrl + newFolder)
